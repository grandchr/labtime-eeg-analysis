% EEGLAB history file generated on the 17-Apr-2024
% ------------------------------------------------

inputDatapathRoot = 'C:\\YourInputPath\\Data\\AuditoryOddball'
folderList = dir(fullfile(inputDatapathRoot, 'sub-*'));
 
for folderIndex = 1:length(folderList)
    [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
    currentFolder = folderList(folderIndex)
    dataFolder = fullfile(inputDatapathRoot, currentFolder.name, 'eeg');
    inputFile = dir(fullfile(dataFolder, strcat(currentFolder.name, '_task-P300_run-2_eeg.set')));
	
	EEG = pop_loadset('filename',inputFile.name,'filepath',inputFile.folder);
	[ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
	pop_eegplot( EEG, 1, 1, 1);
	EEG = pop_select( EEG, 'rmchannel',{'EXG1','EXG2','EXG3','EXG4','EXG5','EXG6','EXG7','EXG8','GSR1','GSR2','Erg1','Erg2','Resp','Plet','Temp'});
	[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'gui','off'); 
	pop_eegplot( EEG, 1, 1, 1);
	EEG = pop_eegfiltnew(EEG, 'locutoff',1,'plotfreqz',1);
	[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 2,'gui','off'); 
	pop_eegplot( EEG, 1, 1, 1);
	EEG = pop_reref( EEG, []);
	[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 3,'gui','off'); 
	EEG = pop_clean_rawdata(EEG, 'FlatlineCriterion',5,'ChannelCriterion',0.8,'LineNoiseCriterion',4,'Highpass','off','BurstCriterion',20,'WindowCriterion',0.25,'BurstRejection','on','Distance','Euclidian','WindowCriterionTolerances',[-Inf 7] );
	[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 4,'gui','off'); 
	EEG = pop_runica(EEG, 'icatype', 'picard', 'maxiter',500);
	[ALLEEG, EEG, CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);
	EEG = pop_iclabel(EEG, 'default');
	[ALLEEG, EEG, CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);
	pop_eegplot( EEG, 0, 1, 1);
	pop_selectcomps(EEG, [1:10] );
	EEG = pop_icflag(EEG, [NaN NaN;0.9 1;0.9 1;NaN NaN;NaN NaN;NaN NaN;NaN NaN]);
	[ALLEEG, EEG, CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);
	EEG = pop_subcomp( EEG, [], 0);
	[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 5,'gui','off'); 
	EEG = pop_epoch( EEG, {  'oddball_with_reponse'  }, [-1  2], 'newname', 'Oddball', 'epochinfo', 'yes');
	[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 6,'gui','off'); 
	EEG = pop_rmbase( EEG, [-300 0] ,[]);
	[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 7,'gui','off'); 
	EEG = pop_saveset( EEG, 'filename','odball.set','filepath',dataFolder.folder);
	[ALLEEG, EEG, CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);
	[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 8,'retrieve',6,'study',0); 
	EEG = pop_epoch( EEG, {  'standard'  }, [-1  2], 'newname', 'Standard', 'epochinfo', 'yes');
	[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 6,'gui','off'); 
	EEG = pop_rmbase( EEG, [-300 0] ,[]);
	[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 9,'gui','off'); 
	EEG = pop_saveset( EEG, 'filename','standard.set','filepath',dataFolder.folder);
	[ALLEEG, EEG, CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);
end
