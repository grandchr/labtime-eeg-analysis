# labtime-eeg-analysis

Présentation Psy.code.logist sur l'Analyse EEG avec EEGLAB (1h30) et materiel associé (Bibliographie, Données, Code).

## EEGLAB

- [ ] Si vous disposez de Matlab, installer la version [EEGLAb v2023.1 Matlab](https://sccn.ucsd.edu/eeglab/download/daily/eeglab2023.1.zip) 
- [ ] Sinon installez la version EEGLAb v2023.1 Standalone disponible pour [Windows](https://sccn.ucsd.edu/eeglab/download/daily/compiled/eeglab_compiled_windows_2023.1.zip) ou [MAC](https://sccn.ucsd.edu/eeglab/download/daily/compiled/eeglab_compiled_macos_2023.1.zip)

## Data

Les données à utiliser sont dans le répertoire /Data/AuditoryOddball_lite qui contient un dossier par participant. Commencer avec le fichier sub-001_task-P300_run-2_eeg.set .
Ce set de données est issu d'un set complet avec 3 runs par participant téléchargeable [ici](https://doi.org/10.18112/openneuro.ds003061.v1.1.1).

## Scripts
Contient :
- [ ] eeglabhist_demo.materiel : session history script de la démo 
- [ ] snippet_for_folderlist.m : snippet qui ermet de parcourir la liste des dossiers des participants 
- [ ] preprocessing_allfiles_script.m : exemple de script complet qui permet d'appliquer le pipeline à l'ensemble des participants

## Diapositives

Les diapos de la présentation en version .pptx ou .pdf .

## Bibliographie

Contient les pdf des références citées.

- Delorme, A. (2023). EEG is better left alone. Scientific Reports, 13(1), 2372. https://doi.org/10.1038/s41598-023-27528-0
- Delorme, A., & Makeig, S. (2004). EEGLAB : An open source toolbox for analysis of single-trial EEG dynamics including independent component analysis. Journal of Neuroscience Methods, 134(1), 9‑21. https://doi.org/10.1016/j.jneumeth.2003.10.009
- Delorme, A., Miyakoshi, M., Jung, T.-P., & Makeig, S. (2015). Grand average ERP-image plotting and statistics : A method for comparing variability in event-related single-trial EEG activities across subjects and conditions. Journal of neuroscience methods, 250, 3‑6. https://doi.org/10.1016/j.jneumeth.2014.10.003
- Delorme, A., Palmer, J., Onton, J., Baehr, R., & Makeig, S. (2012). Independent EEG Sources Are Dipolar. PLoS ONE, 7(2), e30135. https://doi.org/10.1371/journal.pone.0030135
- Grandchamp, R., & Delorme, A. (2011). Single-trial normalization for event-related spectral decomposition reduces sensitivity to noisy trials. Frontiers in Perception Science, 2, 236. https://doi.org/10.3389/fpsyg.2011.00236
- Makeig, S., Debener, S., Onton, J., & Delorme, A. (2004). Mining event-related brain dynamics. Trends in Cognitive Sciences, 8(5), 204‑210. https://doi.org/10.1016/j.tics.2004.03.008
- Martínez-Cancino, R., Delorme, A., Truong, D., Artoni, F., Kreutz-Delgado, K., Sivagnanam, S., Yoshimoto, K., Majumdar, A., & Makeig, S. (2021). The open EEGLAB portal Interface : High-Performance computing with EEGLAB. NeuroImage, 224, 116778. https://doi.org/10.1016/j.neuroimage.2020.116778
- Mullen, T., Delorme, A., Kothe, C., & Makeig, S. (s. d.). An Electrophysiological Information Flow Toolbox for EEGLAB.
- Pernet, C. R., Martinez-Cancino, R., Truong, D., Makeig, S., & Delorme, A. (2021). From BIDS-Formatted EEG Data to Sensor-Space Group Results : A Fully Reproducible Workflow With EEGLAB and LIMO EEG. Frontiers in Neuroscience, 14, 610388. https://doi.org/10.3389/fnins.2020.610388
